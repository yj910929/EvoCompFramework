################################################################################
#   ECFRAMEWORK                                                                #
#   AUTHOR:         Timothy Matthew Threadgold                                 #
#   DESCRIPTION:                                                               #
#   This is intended to be a generalised framework for                         # 
#   programming evolutionary algorithms and for testing standard opeartors     #
#   against test problems.                                                     #
#   DATE:           14/11/2017                                                 #
################################################################################

Current Version: 0.1.17